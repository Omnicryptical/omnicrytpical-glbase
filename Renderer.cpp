#include "Renderer.h"
#include "Object.h"

Renderer::Renderer()
{

    GLCall(glGenVertexArrays(1, &vertexArrayObject));
    GLCall(glBindVertexArray(vertexArrayObject));

	//Create Vertex Buffer
    glGenBuffers(1, &buffer);

	// Generate a buffer for the indices
    glGenBuffers(1, &indexbuffer);

    stbi_set_flip_vertically_on_load(1);
    texture0 = stbi_load("Assets/normalmap.png", &texWidth, &texHeight, &BPP, 4);
    if (texture0 == nullptr)std::cout << "FAIles immage";
    GLCall(glGenTextures(1, &texNum));
    GLCall(glBindTexture(GL_TEXTURE_2D, texNum));
    GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
    GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT));
    GLCall(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT));
    GLCall(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, texWidth, texHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture0));
    GLCall(glBindTexture(GL_TEXTURE_2D, 0));
    if (texture0 != nullptr) stbi_image_free(texture0);

    loadShaders("vertex.glsl","fragment.glsl");

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

    //glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);
    //glFrontFace(GL_CCW);

	//Set Up Camera
	camera = new Camera(0, 0, 10);
}

Renderer::~Renderer()
{
	delete camera;


	glDetachShader(program, vertexShader);
	glDetachShader(program, fragmentShader);

	glDisableVertexAttribArray(0);
	glDeleteBuffers(1, &buffer);
}

void Renderer::drawTriangle(const Vec2<float>& v1, const Vec2<float>& v2, const Vec2<float>& v3)
{
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

	float Positions[] =
	{
		v1.x, v1.y,
		v2.x, v2.y,
		v3.x, v3.y
	};

	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 6, Positions, GL_DYNAMIC_DRAW);

	glUseProgram(program);

	GLCall(glDrawArrays(GL_TRIANGLES, 0, 3));
}

void Renderer::drawDrawable(Drawable* drawable)
{
    batch.draw(drawable);
}

void Renderer::drawFocus()
{
    Object focus(
		{ camera->xfocus, camera->yfocus, camera->zfocus },
		0.05,
		{ 1.0, 0.0, 0.0 });

    focus.Render(this);
}

void Renderer::renderPresent()
{
    GLCall(glBindBuffer(GL_ARRAY_BUFFER, buffer));
    GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(float)*batch.vertexCount*batch.vertexSize, batch.vertices, GL_DYNAMIC_DRAW));

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 11*sizeof(float), 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 11*sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 11*sizeof(float), (void*)(6*sizeof(float)));
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 11*sizeof(float), (void*)(8*sizeof(float)));

    GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexbuffer));
    GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, batch.indexCount * sizeof(unsigned int), batch.indices, GL_DYNAMIC_DRAW));

    GLCall(glUseProgram(program));
    bound = true;

    GLCall(glActiveTexture(GL_TEXTURE0));
    GLCall(glBindTexture(GL_TEXTURE_2D, texNum));

    unsigned int CamMatID = glGetUniformLocation(program, "Camera");
    glUniformMatrix4fv(CamMatID, 1, GL_FALSE, &camera->view[0][0]);
    unsigned int texID = glGetUniformLocation(program, "u_Texture");
    glUniform1i(texID, 0);
    unsigned int lightID = glGetUniformLocation(program, "u_lightingDirection");
    glUniform3f(lightID, lightingDirection.x, lightingDirection.y, lightingDirection.z);

    GLCall(glDrawElements(GL_TRIANGLES, batch.indexCount, GL_UNSIGNED_INT, 0));

    batch.flush();
}

void Renderer::shiftCamera(float x, float y, float z)
{
    camera->shiftCamera(x, y, z);
}

void Renderer::moveCamera(float d)
{
    camera->moveCamera(d);
}

void Renderer::strafeCamera(float d)
{
    camera->strafeCamera(d);
}

void Renderer::rotateCameraPitch(float theta)
{
    camera->rotateCameraPitch(theta);
}

void Renderer::rotateCameraYaw(float theta)
{
    camera->rotateCameraYaw(theta);
}

void Renderer::loadShaders(std::string vFilePath, std::string fFilePath)
{
	// Read our shaders into the appropriate buffers
	std::string vertexSource = LoadTextFile(vFilePath);
	std::string fragmentSource = LoadTextFile(fFilePath);

    if (vertexSource == ""){
        std::cout << "Vertex Shader is null" <<std::endl;
    }
    if (fragmentSource == ""){
        std::cout << "Fragment Shader is null" <<std::endl;
    }

	vertexShader = glCreateShader(GL_VERTEX_SHADER);

	const char* source = (const char*)vertexSource.c_str();
	glShaderSource(vertexShader, 1, &source, 0);

	// Compile the vertex shader
	GLCall(glCompileShader(vertexShader));

	int isCompiled = 0;
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		int maxLength = 0;
		glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<char> infoLog(maxLength);
		glGetShaderInfoLog(vertexShader, maxLength, &maxLength, &infoLog[0]);

		glDeleteShader(vertexShader);

		for (char c : infoLog) {
			std::cout << c;
		}
		std::cout << "\n\n";
	}

	// Create an empty fragment shader handle
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	// Send the fragment shader source code to GL
	// Note that std::string's .c_str is NULL character terminated.
	source = (const char*)fragmentSource.c_str();
	glShaderSource(fragmentShader, 1, &source, 0);

	// Compile the fragment shader
	GLCall(glCompileShader(fragmentShader));
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		int maxLength = 0;
		glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<char> infoLog(maxLength);
		glGetShaderInfoLog(fragmentShader, maxLength, &maxLength, &infoLog[0]);

		glDeleteShader(fragmentShader);

		for (char c : infoLog) {
			std::cout << c;
		}
		std::cout << "\n\n";
	}

	// Now time to link them together into a program.
	// Get a program object.
    program = glCreateProgram();

	// Attach our shaders to our program
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);

	// Link our program
    GLCall(glLinkProgram(program));

	int isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE)
	{
		int maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		// The maxLength includes the NULL character
		std::vector<char> infoLog(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);

		for (char c : infoLog) {
			std::cout << c;
		}
		std::cout << "\n\n";

		// We don't need the program anymore.
		glDeleteProgram(program);
		// Don't leak shaders either.
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
	}
}
