TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS        += -lglfw -lGLEW -pthread -ldl -lGLU -lGL -lrt -lXrandr -lXcursor -lXxf86vm -lXi -lXinerama -lX11 -lGL -lGLU

SOURCES += \
    Camera.cpp \
    Dice.cpp \
    main.cpp \
    Engine.cpp \
    Renderer.cpp \
    Drawable.cpp \
    Batch.cpp \
    stb_image.cpp

HEADERS += \
    Camera.h \
    Dice.h \
    Engine.h \
    GLError.h \
    FrameTimer.h \
    GLInclude.h \
    Keyboard.h \
    LoadFiles.h \
    Mouse.h \
    Renderer.h \
    Drawable.h \
    Mathstructs.h \
    Object.h \
    Batch.h \
    stb_image.h
