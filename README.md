This project is just a recent, roughly thrown-together exploration of basic 3D graphics using OpenGL. 3D shapes are automatically generated and rendered using normal map textures. Shapes move around in rotational and translational motions and the user controls a camera that moves through the scene.

Currently the project is only set up to be imported into the QtCreator IDE, so there is no easy way to build it otherwise.

![IMAGE_DESCRIPTION](Screenshots/glbase.png)
