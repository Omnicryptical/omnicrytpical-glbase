#version 330 core
in vec3 fragmentColor;
in vec2 texCoords;
in vec3 norm;
out vec3 color;

uniform vec3 u_lightingDirection;
uniform sampler2D u_Texture;

void main() {
    vec3 texColor = texture(u_Texture, texCoords).rgb;
    vec3 finalColor = fragmentColor*max(dot((-norm-3*texColor)/2, u_lightingDirection), 0.1+texColor.b/15.0);
	color = finalColor;
}
