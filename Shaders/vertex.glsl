#version 330 core

layout(location = 0) in vec3 Position;
layout(location = 1) in vec3 Color;
layout(location = 2) in vec2 textureCoords;
layout(location = 3) in vec3 normal;

out vec3 fragmentColor;
out vec2 texCoords;
out vec3 norm;

uniform mat4 Camera;

void main() 
{
	fragmentColor = Color;
    texCoords = textureCoords;
    norm = normal;
	gl_Position = Camera * vec4(Position, 1.0);
}
